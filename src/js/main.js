$(document).ready(function () {

	headerScroll()

	headerSearch()

	popularSlider()

	mainGallery()

	headerMenu()

	dropLink()

	disableZoomOnMobile()

	scrollAnchor()

	typeToggle()

	togglePlan()
});

const togglePlan = () => {

	$('.card__image-type-item').on('click', function () {

		let src = $(this).attr('data-src')

		$('.card__image-main img').attr('src', src)
		
		$('.card__image-type-item').fadeIn(0)
		$(this).fadeOut(0)


	})
}

const typeToggle = () => {

	$('.card__tour-toggle-button').on('click', function () {

		$(this).parents('.card__tour-toggle').toggleClass('card__tour-toggle--active')
	})
}

const disableZoomOnMobile = () => {

	$("input[type=text], textarea, selectize-input").on({ 'touchstart' : function() {
        zoomDisable();
    }});

    $("input[type=text], textarea, selectize-input").on({ 'touchend' : function() {
        setTimeout(zoomEnable, 500);
    }});

    if ($(window).width() < 768) {
    	zoomDisable()
    }

    function zoomDisable(){
        $('head meta[name=viewport]').remove();
        $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />');
    }
    function zoomEnable(){
        $('head meta[name=viewport]').remove();
        $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1" />');
    }
}

const headerSearch = () => {

	let form = $('.header__search-form')

	$('body').on('click', '.header__search-icon', function () {

		form.addClass('is-opened')

		setTimeout(() => {
			$('.header__search-input').focus()
		}, 300)

		return false
	})

	$(document).on('click touchend', function (e) {

		if (form.hasClass('is-opened')) {

			var block = $('.header__search-form')

			if (!block.is(e.target) && block.has(e.target).length === 0) {
				form.removeClass('is-opened')

				return false
			}
		}
	})
}

const popularSlider = () => {

	$('.popular__slider').each(function () {

		let scene = $(this)
		let navigation = scene.parents('.popular__scene').find('.popular__nav')

		scene.owlCarousel({
			items: 3,
			nav: true,
			navText: '',
			dots: false,
			margin: 20,
			navElement: 'a',
			navContainer: navigation,
			navClass: ['popular__arr popular__arr--prev','popular__arr popular__arr--next'],
			responsive: {
				0: {
					items: 1
				},
				768: {
					items: 2,
					margin: 0
				}, 
				1280: {
					items: 3,
					margin: 0
				},
				1600: {
					margin: 20
				}
			}
		})
	})
}

const mainGallery = () => {

	let scene = $('.gallery__main-scene')

	const owlTranslate = (e) => {
		$('.gallery__thumbs-item').eq(e.item.index).addClass('active')
		$('.gallery__thumbs-item').eq(e.item.index).siblings().removeClass('active')
	}

	scene.owlCarousel({
		items: 1,
		nav: true,
		navText: '',
		dots: false,
		navClass: ['gallery__main-arr gallery__main-arr--prev', 'gallery__main-arr gallery__main-arr--next'],
		navElement: 'a',
		onTranslate: owlTranslate
	})

	$('.gallery__thumbs-item').on('click', function () {

		let that = $(this)
		let index = that.index()

		scene.trigger('to.owl.carousel', index)

		that.addClass('active')
		that.siblings().removeClass('active')
	})
}

const headerScroll = () => {

	let scrolled, lastScrollTop = 0

	$(window).on('scroll load', function () {

		scrolled = $(window).scrollTop()

		if (scrolled > lastScrollTop && scrolled > 0){
			$('.header').addClass('header--hidden')
		} else if (scrolled > 0) {
			$('.header').removeClass('header--hidden')
		}

		if (scrolled > 0) {
			$('.header').addClass('header--fixed')
		} else {
			$('.header').removeClass('header--fixed')
		}

		lastScrollTop = scrolled
	})
}

const headerMenu = () => {

	$('.header__burger').on('click', function () {

		$(this).toggleClass('header__burger--opened')

		$('.header__menu').toggleClass('header__menu--opened')

		return false
	})

	$(document).on('click touchend', function (e) {

		if ($('.header__menu').hasClass('header__menu--opened')) {

			var block = $('.header__menu')

			if (!block.is(e.target) && block.has(e.target).length === 0) {

				$('.header__menu').removeClass('header__menu--opened')
				$('.header__burger').removeClass('header__burger--opened')

				return false
			}
		}
	})
}

const dropLink = () => {

	$('.header__nav-link--drop').on('click touchstart', function () {

		if (!$(this).hasClass('clicked')) {

			$(this).addClass('clicked')

			$(this).next('.header__nav-drop').fadeIn()

			return false
		}
	})
}

const scrollAnchor = () => {

	$('.welcome__features-scroll').on('click', function () {

		let offset = $('.complexes').offset().top

		$('html,body').animate({
          scrollTop: offset
        }, 800);
	})
}








